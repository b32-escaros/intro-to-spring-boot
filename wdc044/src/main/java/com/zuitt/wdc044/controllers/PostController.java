package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {
  @Autowired
  PostService postService;

  // Create a new post
  @RequestMapping(value = "/posts", method = RequestMethod.POST)
  public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
    postService.createPost(stringToken, post);
    return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
  }

  // Retrieve all posts
  @RequestMapping(value = "/posts", method = RequestMethod.GET)
  public ResponseEntity<Object> getPosts() {
    return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
  }

  // Update a post
  @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
  public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
    return postService.updatePost(postid, stringToken, post);
  }

  // Delete a post
  @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
  public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken) {
    return postService.deletePost(postid, stringToken);
  }

  // Get all posts of a user
  @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
  public ResponseEntity<Object> getMyPosts(@RequestHeader(value = "Authorization") String stringToken) {
    return postService.getPostsByUser(stringToken);
  }
}
